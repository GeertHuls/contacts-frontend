import {DataState} from "./DataState";
export interface ApplicationState {
    data: DataState;
}