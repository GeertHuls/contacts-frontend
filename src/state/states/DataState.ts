import {AuthenticationDataState} from "./AuthenticationDataState";
export interface DataState {
    authentication: AuthenticationDataState;
}