export interface AuthenticationDataState {
    isAuthenticated: boolean;
    username: string;
    token: string;
}