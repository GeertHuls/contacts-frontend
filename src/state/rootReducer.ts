import {combineReducers} from "@ngrx/store";
import {authenticationReducer} from "./reducers/authentication.reducer";
export let rootReducer = {
    data: combineReducers({
        authentication: authenticationReducer
    })
};