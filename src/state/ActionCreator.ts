import {Action} from "@ngrx/store";
import {ActionTypes} from "./ActionTypes";
import {Account} from "../common/domain/Account";
export class ActionCreator {

    public static setAuthentication(account: Account): Action {
        return {
            type: ActionTypes.SET_AUTHENTICATION,
            payload: account
        };
    }

    public static clearAuthentication(): Action {
        return {
            type: ActionTypes.CLEAR_AUTHENTICATION
        };
    }

}