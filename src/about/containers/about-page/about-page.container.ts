import {Component} from "@angular/core";
@Component({
    selector: "about-page",
    template: `
        <div class="about">
            <h3>Angular 2 course</h3>
            <img src="https://angular.io/resources/images/logos/angular2/angular.png" />
        </div>
     `,
    styles: [require("./about-page.container.scss")]
})
export class AboutPageContainer {
}
