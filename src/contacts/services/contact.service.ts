import {Http, Response} from "@angular/http";
import {API_URL} from "../../configuration";
import {Observable} from "rxjs/Observable";
import {Contact} from "../domain/Contact";
import {Injectable} from "@angular/core";

@Injectable()
export class ContactService {
    constructor(private http: Http) {

    }

    public fetchAllContacts(): Observable<Contact[]> {
        return this.http.get(`${API_URL}/contact`)
            .map((response: Response) => response.json());
    }

    public save(contact: Contact): Observable<Contact> {
        return this.http.post(`${API_URL}/contact`, contact)
            .map((response: Response) => response.json());
    }

    public update(contact: Contact): Observable<Contact> {
        return this.http.put(`${API_URL}/contact/${contact.id}`, contact)
            .map((response: Response) => response.json());
    }

    public getContact(id: number): Observable<Contact> {
        return this.http.get(`${API_URL}/contact/${id}`)
            .map((response: Response) => response.json());
    }

    public deleteContact(id: number): Observable<any> {
        return this.http.delete(`${API_URL}/contact/${id}`);
    }
}