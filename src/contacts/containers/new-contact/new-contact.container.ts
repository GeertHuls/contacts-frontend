import {Component, OnDestroy} from "@angular/core";
import {Contact} from "../../domain/Contact";
import {ContactsSandbox} from "../../contacts.sandbox";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
@Component({
    selector: "contact-new",
    template: `
        <div class="container">
            <contact-detail [contact]="contact" 
                (onSave)="saveContact($event)"
                (onCancel)="closeContact()"></contact-detail>
        </div>
    `
})
export class NewContactContainer implements OnDestroy {
    public contact: Contact = new Contact();
    private subscription: Subscription;

    constructor(private contactsSandbox: ContactsSandbox, private router: Router) {

    }

    public ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public saveContact(contact: Contact): void {
        this.subscription = this.contactsSandbox.saveContact(contact)
            .subscribe((newContact: Contact) => this.router.navigate([`/contacts/edit/${newContact.id}`]));
    }

    public closeContact(): void {
        this.router.navigate(["/contacts"]);
    }

}