import {Component, Input} from "@angular/core";
import {Contact} from "../../domain/Contact";
@Component({
    selector: "contact",
    template: `
        <div class="contact">
            <span class="contact-name">{{contact.name}}</span>
        </div>
    `,
    styles: [require("./contact.component.scss")]
})
export class ContactComponent {
    @Input() contact: Contact;
}