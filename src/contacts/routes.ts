import {RouterModule} from "@angular/router";
import {ContactsSearchContainer} from "./containers/contacts-search/contacts-search.container";
import {NewContactContainer} from "./containers/new-contact/new-contact.container";
import {EditContactContainer} from "./containers/edit-contact/edit-contact.container";
import {AuthenticatedGuard} from "../auth/guards/authenticated.guard";
export const routes = [
    { path: "contacts", component: ContactsSearchContainer, canActivate: [AuthenticatedGuard]},
    { path: "contacts/add", component: NewContactContainer, canActivate: [AuthenticatedGuard]},
    { path: "contacts/edit/:id", component: EditContactContainer, canActivate: [AuthenticatedGuard]}
];

export const routing = RouterModule.forChild(routes);
