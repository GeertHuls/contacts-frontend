import {NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {routing} from "./routes";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {ContactsSearchContainer} from "./containers/contacts-search/contacts-search.container";
import {ContactsSandbox} from "./contacts.sandbox";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ContactService} from "./services/contact.service";
import {ContactDetailComponent} from "./components/contact-detail/contact-detail.component";
import {ContactListComponent} from "./components/contact-list/contact-list.component";
import {ContactComponent} from "./components/contact/contact.component";
import {NewContactContainer} from "./containers/new-contact/new-contact.container";
import {EditContactContainer} from "./containers/edit-contact/edit-contact.container";
import {SharedModule} from "../common/index";

@NgModule({
    imports: [RouterModule, CommonModule, HttpModule, FormsModule, ReactiveFormsModule, routing, SharedModule],
    declarations: [ContactsSearchContainer, NewContactContainer, EditContactContainer, ContactDetailComponent,
        ContactComponent, ContactListComponent],
    providers: [ContactsSandbox, ContactService]
})
export class ContactsModule {
}

