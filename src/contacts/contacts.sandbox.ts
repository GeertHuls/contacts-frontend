import {Injectable} from "@angular/core";
import {Contact} from "./domain/Contact";
import {ContactService} from "./services/contact.service";
import {Observable} from "rxjs/Observable";
@Injectable()
export class ContactsSandbox {

    public contacts: Observable<Contact[]>;

    constructor(private contactService: ContactService) {
        this.contacts = this.contactService.fetchAllContacts();
    }

    public saveContact(contact: Contact): Observable<Contact> {
        return this.contactService.save(contact);
    }

    public updateContact(contact: Contact): Observable<Contact> {
        return this.contactService.update(contact);
    }

    public getContact(id: number): Observable<Contact> {
        return this.contactService.getContact(id);
    }

    public deleteContact(id: number) {
        return this.contactService.deleteContact(id);
    }
}