import {TranslateModule, TranslateLoader} from "ng2-translate";
import {CustomTranslateLoader} from "./i18n/CustomTranslateLoader";
import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
@NgModule({
    imports: [
        BrowserModule,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useClass: CustomTranslateLoader
        })
    ],

    exports: [TranslateModule]
})
export class SharedModule {
}