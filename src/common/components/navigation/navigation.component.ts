import {Component, Output, EventEmitter, Input} from "@angular/core";

@Component({
    selector: "navigation",
    styles: [require("./navigation.component.scss")],
    template: `
        <div class="navigation">
            <span class="navigation__brand">
                <a [routerLink]="['/']">{{'app.name' | translate}}</a>
            </span>
            <div class="navigation__user">
                <span class="navigation__username">{{username}}</span>
                <button class="navigation__logout" (click)="logout()">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> {{'label.logout' | translate}}
                </button>
                <a [routerLink]="['/about']" class="navigation__about">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                </a>
                <button (click)="toggleLanguage()">
                    <i class="fa fa-language" aria-hidden="true"></i>
                </button>
            </div>
        </div>
`
})
export class NavigationComponent {
    @Input() username: string;
    @Output() onLogout: EventEmitter<any> = new EventEmitter();
    @Output() onToggleLanguage: EventEmitter<any> = new EventEmitter();


    constructor() {
    }

    public logout(): void {
        this.onLogout.emit(null);
    }

    public toggleLanguage(): void {
        this.onToggleLanguage.emit(null);
    }
}