import {Title} from "@angular/platform-browser";
import {Component, ViewEncapsulation} from "@angular/core";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";
import {Router} from "@angular/router";
import {AppSandbox} from "../../app.sandbox";
import {Observable} from "rxjs/Observable";
import {TranslateService} from "ng2-translate";
@Component({
    selector: "application",
    encapsulation: ViewEncapsulation.None,
    styles: [require("./application.container.scss")],
    providers: [Title, TranslateService],
    template: `
        <navigation *ngIf="isAuthenticated$|async"
            [username]="username$|async"
            (onLogout)="logout()"
            (onToggleLanguage)="toggleLanguage()"></navigation>           
        <router-outlet></router-outlet>
    `
})
export class ApplicationContainer {
    public isAuthenticated$: Observable<boolean>;
    public username$: Observable<string>;

    constructor(private title: Title, private router: Router, private appSandbox: AppSandbox, private translate: TranslateService) {
        translate.addLangs(["en", "nl"]);
        translate.setDefaultLang("en");
        translate.use("en");

        this.title.setTitle(translate.instant("app.name"));
        this.isAuthenticated$ = this.appSandbox.isAuthenticated$;
        this.username$ = this.appSandbox.username$;
    }

    public logout(): void {
        this.appSandbox.logout();
        this.router.navigate(["/login"]);
    }

    public toggleLanguage(): void {
        let lang: string = this.translate.getLangs().find(l => l !== this.translate.currentLang);
        this.translate.use(lang);
    }
}