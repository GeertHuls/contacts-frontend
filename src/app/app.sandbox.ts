import {Injectable} from "@angular/core";
import {AuthenticationService} from "../auth/services/authentication.service";
import {Observable} from "rxjs/Observable";
import {ApplicationState} from "../state/states/ApplicationState";
import {Store} from "@ngrx/store";
@Injectable()
export class AppSandbox {

    public isAuthenticated$: Observable<boolean>;
    public username$: Observable<string>;

    constructor(private authenticationService: AuthenticationService, private store: Store<ApplicationState>) {
        this.isAuthenticated$ = this.store.select(state => state.data.authentication.isAuthenticated);
        this.username$ = this.store.select(state => state.data.authentication.username);
        this.authenticationService.restoreAuthentication();
    }

    public logout(): void {
        this.authenticationService.logout();
    }
}