import {RouterModule} from "@angular/router";
import {AuthenticatedGuard} from "../auth/guards/authenticated.guard";
export const routes = [
    {path: "", redirectTo: "/contacts", pathMatch: "full", canActivate: [AuthenticatedGuard]}
];
export const routing = RouterModule.forRoot(routes);
