import {Injectable} from "@angular/core";
import {Credentials} from "./domain/Credentials";
import {AuthenticationService} from "./services/authentication.service";
import {Observable} from "rxjs/Observable";
import {Account} from "../common/domain/Account";

@Injectable()
export class AuthSandbox {

    constructor(private authenticationService: AuthenticationService) {

    }

    public login(credentials: Credentials): Observable<Account> {
        return this.authenticationService.authenticate(credentials);
    }

    public logout(): void {
        this.authenticationService.logout();
    }

    public register(credentials: Credentials): Observable<any> {
        return this.authenticationService.register(credentials);
    }
}