import {Component, Output, EventEmitter} from "@angular/core";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Credentials} from "../../domain/Credentials";

@Component({
    selector: "register",
    styles: [require("./register.component.scss")],
    template: `            
            <form [formGroup]="registerForm" class="register-form" (submit)="onSubmit()">
                <div class="register-logo">
                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                </div>
                <input type="text" 
                    name="username" 
                    placeholder="{{'placeholder.username' | translate}}"
                    formControlName="username">
                <input type="password" 
                    name="password" 
                    placeholder="{{'placeholder.password' | translate}}"
                    formControlName="password">
                <div class="button-group">
                    <button class="btn btn-register" type="submit" [disabled]="!registerForm.valid">
                        {{'label.register' | translate}}
                    </button>
                    <button class="btn btn-default" type="button" (click)="cancel()">
                        {{'label.cancel' | translate}}
                    </button>
                </div>
            </form>
        `
})
export class RegisterComponent {
    @Output() onRegister: EventEmitter<Credentials> = new EventEmitter<Credentials>();
    @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

    public registerForm: FormGroup;

    constructor(private formBuilder: FormBuilder) {
        this.registerForm = formBuilder.group({
            username: ["", Validators.required],
            password: ["", Validators.required]
        });
    }

    public onSubmit() {
        let credentials: Credentials = this.registerForm.value;
        this.onRegister.emit(credentials);
    }

    public cancel() {
        this.onCancel.emit(null);
    }

}