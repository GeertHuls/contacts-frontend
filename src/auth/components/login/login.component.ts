import {Component, Output, EventEmitter} from "@angular/core";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Credentials} from "../../domain/Credentials";

@Component({
    selector: "login",
    styles: [require("./login.component.scss")],
    template: `            
            <form [formGroup]="loginForm" class="login-form" (submit)="onSubmit()">
                <div class="login-logo">
                    <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                </div>
                <input type="text" 
                    name="username" 
                    placeholder="{{'placeholder.username' | translate}}"
                    formControlName="username">
                <input type="password" 
                    name="password" 
                    placeholder="{{'placeholder.password' | translate}}"
                    formControlName="password">
                <button class="btn" type="submit" [disabled]="!loginForm.valid">{{'label.login' | translate}}</button>
                <span class="login--registration">
                    {{'label.not.registered' | translate}} <a class="login--registration__link" href="javascript:void 0" 
                    (click)="register()">{{'label.create.account' | translate}}</a>
                </span>
            </form>
        `
})
export class LoginComponent {
    @Output() onLogin: EventEmitter<Credentials> = new EventEmitter<Credentials>();
    @Output() onLogout: EventEmitter<any> = new EventEmitter<any>();
    @Output() onRegister: EventEmitter<any> = new EventEmitter<any>();

    public loginForm: FormGroup;

    constructor(private formBuilder: FormBuilder) {
        this.loginForm = formBuilder.group({
            username: ["", Validators.required],
            password: ["", Validators.required]
        });
    }

    public onSubmit() {
        let credentials: Credentials = this.loginForm.value;
        this.onLogin.emit(credentials);
    }

    public register() {
        this.onRegister.emit(null);
    }

}