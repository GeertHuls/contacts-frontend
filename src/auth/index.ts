import {NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {routing} from "./routes";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthContainer} from "./containers/auth.container";
import {AuthSandbox} from "./auth.sandbox";
import {AuthenticationService} from "./services/authentication.service";
import {RegisterComponent} from "./components/register/register.component";
import {LoginComponent} from "./components/login/login.component";
import {SharedModule} from "../common/index";

@NgModule({
    imports: [RouterModule, CommonModule, HttpModule, FormsModule, ReactiveFormsModule, routing, SharedModule],
    declarations: [AuthContainer, LoginComponent, RegisterComponent],
    providers: [AuthSandbox, AuthenticationService]
})
export class AuthModule {
}

