require("dotenv").config();

var StringReplacePlugin = require("string-replace-webpack-plugin");
var API_URL = process.env.API_URL;
module.exports = [
  {
    test: /configuration.ts$/,
    loader: StringReplacePlugin.replace({
      replacements: [
        {
          pattern: '%API_URL%',
          replacement: function () {
            return API_URL
          }
        }
      ]
    })
  }
];