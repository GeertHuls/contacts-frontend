import {async, TestBed, ComponentFixture} from "@angular/core/testing";
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {ContactComponent} from "../../../../../src/contacts/components/contact/contact.component";
import {Contact} from "../../../../../src/contacts/domain/Contact";
describe("Test: Contact component", () => {

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ContactComponent],
            schemas: [NO_ERRORS_SCHEMA]
        });
        TestBed.compileComponents();
    }));

    it("Should display the name of the passed contact", () => {
        const fixture: ComponentFixture<ContactComponent> = TestBed.createComponent(ContactComponent);
        const element = fixture.nativeElement;
        fixture.componentInstance.contact = new Contact(0, "FooBar");
        fixture.detectChanges();

        expect(element.querySelector(".contact-name").innerText).toEqual("FooBar");
    });
});