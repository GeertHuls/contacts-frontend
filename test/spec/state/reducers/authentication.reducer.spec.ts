import {authenticationReducer} from "../../../../src/state/reducers/authentication.reducer";
import {AuthenticationDataState} from "../../../../src/state/states/AuthenticationDataState";
import {ActionCreator} from "../../../../src/state/ActionCreator";
import {Account} from "../../../../src/common/domain/Account";

let deepfreeze = require("deep-freeze");

describe("Test: Authentication reducer", () => {

    describe("When authentication is set", () => {
        it("should return a new instance with the correct state", () => {
            let initialState: AuthenticationDataState = {
                isAuthenticated: false,
                token: null,
                username: null
            };
            let account: Account = new Account("John", "token");

            deepfreeze(initialState);

            let newState = authenticationReducer(initialState, ActionCreator.setAuthentication(account));

            expect(newState).not.toBe(initialState);
            expect(newState.username).toEqual(account.username);
            expect(newState.token).toEqual(account.token);
            expect(newState.isAuthenticated).toBeTruthy();

        });
    });

    describe("When authentication is cleared", () => {
        it("should return a new instance with the correct state", () => {
            let initialState: AuthenticationDataState = {
                isAuthenticated: true,
                token: "token",
                username: "John"
            };

            deepfreeze(initialState);

            let newState = authenticationReducer(initialState, ActionCreator.clearAuthentication());

            expect(newState).not.toBe(initialState);
            expect(newState.username).toBeNull();
            expect(newState.token).toBeNull();
            expect(newState.isAuthenticated).toBeFalsy();

        });
    });

    describe("When an unsupported action is passed", () => {
        it("should return the same state", () => {
            let initialState: AuthenticationDataState = {
                isAuthenticated: true,
                token: "token",
                username: "John"
            };

            deepfreeze(initialState);
            let newState = authenticationReducer(initialState, {type: null});

            expect(newState).toBe(initialState);

        });
    });

});