import {AuthenticatedGuard} from "../../../../src/auth/guards/authenticated.guard";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../../../src/auth/services/authentication.service";
import Spy = jasmine.Spy;
describe("Test: AuthenticationGuard", () => {

    let authenticationGuard: AuthenticatedGuard;
    let router: Router;
    let service: AuthenticationService;

    beforeEach(() => {

        router = jasmine.createSpyObj("Router", ["navigate"]);
        service = jasmine.createSpyObj("AuthenticationService", ["isValidAuthentication"]);
        authenticationGuard = new AuthenticatedGuard(router, service);
    });

    describe("When routing", () => {
        describe("Given the user is authenticated", () => {
            it("should allow the route to continue", () => {
                (<Spy>service.isValidAuthentication).and.returnValue(true);
                let actual = authenticationGuard.canActivate(null, null);
                expect(actual).toBeTruthy();
            });
        });

        describe("Given the user isn't authenticated", () => {
            it("shouldn't allow the route to continue", () => {
                (<Spy>service.isValidAuthentication).and.returnValue(false);
                let actual = authenticationGuard.canActivate(null, null);
                expect(actual).toBeFalsy();
            });
        });
    });
});